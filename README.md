# APKMirror

An unofficial APKMirror client/web app (forked from the original, which is now abandoned).


### Longer Description

An Android app that utilizes a WebView to browse APKMirror.
APKMirror provides APKs, as the name obviously suggests.
This app saves you the trouble of having to open up a browser and visit APKMirror by typing the URL,
and is the sole purpose of this app existing (because who needs stupid boring browsers when
you can create an entire app for a site, amirite?).


### Install

<a href="https://f-droid.org/packages/taco.apkmirror">
    <img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
        alt="Get it on F-Droid" height="100">
</a>
<a href="https://gitlab.com/TacoTheDank/APKMirror/-/jobs/artifacts/develop/download?job=build">
    <img src="https://hike.in/images/hike5.0/apk.png"
		alt="Download the latest build" height="100">
</a>


### Features

- Quick loading (depends on phone; newer models load much faster)
- Ability to choose any download manager
- Clean-ish material design
- Small-ish APK size (remember to clear the cache regularly, as it builds up when downloading APKs)


### Things that would constitute as Anti-Features (that aren't already shown by F-Droid)

- The app itself does NOT contain any ad libraries whatsoever (it is completely FOSS).
However, as anyone who has visited the APKMirror site probably knows, they do display ads.
As this app utilizes a WebView, the site's ads will also end up being displayed in the app.
Remember that they show ads to be able to keep their site up, so try not to think too harshly
of them.


### Contributions

Anyone may contribute (I need better translations, and better code review because
I actually know next to nothing about coding).
Make a pull request with changes to contribute.

##### Contributors (thank you!)

- [vojta-horanek](https://github.com/vojta-horanek): For originally creating this app
- [EvoWizz](https://github.com/EvoWizz): The amazing original icon, help with design, the French translation, and much more
- [ja_som](https://github.com/ja-som): Help and Slovak translation
- [Ninnix96](https://forum.xda-developers.com/member.php?u=6002018): Help with development
- [Fast0n](https://github.com/Fast0n): Italian translation
- [SandroAzalel](https://github.com/SandroAzazel): German translation
- [timschumi](https://github.com/timschumi): German translation
- [berkantkz](https://github.com/berkantkz): Turkish translation
- [hbbxmjbb](https://github.com/hbbxmjbb): Chinese translation
- [io_gh0st](https://forum.xda-developers.com/member.php?u=4275144): Chinese translation
- [nightstorm8x](https://github.com/nightstorm8x): Vietnamese translation
- FonoDoSebas<!--([gonzalobustos](https://github.com/gonzalobustos)?)-->: Spanish translation
- josefill: Hungarian translation
- [xx6600xx](https://github.com/xx6600xx): Arabic translation
- [VladAndroidGamer](https://github.com/VladAndroidGamer): Russian translation
- [Radeox](https://github.com/Radeox): Polish translation


### Screenshots

Getting replacement screenshots for a more accurate representation.


### Credits

I decided to continue maintaining this app myself, as the original author had stopped development on it.

Original app by vojta-horanek: https://github.com/vojta-horanek/APKMirror

The last version (3.5) from vojta-horanek can be found here on XDA Labs:
https://labs.xda-developers.com/store/app/cf.vojtechh.apkmirror


### License

This application is under the GNUv2 General Public License, as licensed by the original creator.

```
APKMirror web app/client
    Copyright (C) 2016-present
        Vojtěch Hořánek
        TacoTheDank

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along
    with this program; if not, write to the Free Software Foundation, Inc.,
    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
```
